<?php
@ini_set('max_execution_time', '300');
@ini_set('output_buffering', 'off');
@ini_set('zlib.output_compression', false);
ignore_user_abort(true);

/* Pure DB */
/* define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "root");
define("DB_NAME", "db"); */

/* WP DB */
define('SHORTINIT', true);
require '../wp-config.php';

require 'dumper.php'; // https://github.com/2createStudio/shuttle-export

new Backup_Script;
class Backup_Script
{
	public $password = "QTQP5H42m3dKu5AX"; // https://passwordsgenerator.net/
	public $exportPaths = array(
		'db' => './db.sql.gz',
		'backup' => './backup_HOST_DATESTAMP.zip'
	);

	public function __construct()
	{
		if (!empty($_GET['download'])) {
			$this->forceDownload();
		}

		/* Remove old backups */
		foreach (glob("*.zip") as $file) {
			unlink($file);
		}

		$this->checkPassword(); // Auth before export

		$this->exportPaths['backup'] = str_replace("HOST", $_SERVER['HTTP_HOST'], $this->exportPaths['backup']);
		$this->exportPaths['backup'] = str_replace("DATESTAMP", date("Y-m-d_H-i-s"), $this->exportPaths['backup']);

		if (!empty($_GET['silent'])) {
			$this->silentExport();
		} else {
			$this->basicExport();
		}
	}

	protected function silentExport()
	{
		if ($this->exportDB()) {
			if ($this->exportFiles()) {
				unlink($this->exportPaths['db']);
				$this->forceDownload();
			}
		}
	}

	protected function basicExport()
	{
		$this->echoImmediately('Preparing to export.<br>');

		$this->echoImmediately('Exporting DB... ');
		if (!$this->exportDB()) {
			die('Couldn\'t dump database.');
		}
		$this->echoImmediately('. Size: ' . $this->getFileSizeFormatted($this->exportPaths['db']) . '.<br>');

		$this->echoImmediately('Exporting Files... ');
		if (!$this->exportFiles()) {
			die('Could not create a zip archive.');
		}
		$this->echoImmediately('. Size: ' . $this->getFileSizeFormatted($this->exportPaths['backup']) . '.<br>');

		unlink($this->exportPaths['db']); // Remove db backup

		$this->echoImmediately('Done.<br>');

		$this->redirectToDownload();
	}

	protected function exportDB()
	{
		try {
			$dumper = Shuttle_Dumper::create(array(
				'host' => DB_HOST,
				'username' => DB_USER,
				'password' => DB_PASSWORD,
				'db_name' => DB_NAME,
			));
			$dumper->dump($this->exportPaths['db']);

		} catch (Shuttle_Exception $e) {
			return false;
		}
		return true;
	}

	protected function exportFiles()
	{
		$archive = new FlxZipArchive;
		if ($archive->open($this->exportPaths['backup'], ZipArchive::CREATE) === true) {
			$archive->addDir('..', 'files');
			$archive->addFile($this->exportPaths['db'], basename($this->exportPaths['db']));
			$archive->close();

			return true;
		}
		return false;
	}

	protected function redirectToDownload()
	{
		$this->echoImmediately('<script>window.location.href = "' . $_SERVER['PHP_SELF'] . '?download=true";</script>');
		exit;
	}

	protected function forceDownload()
	{
		$files = glob("*.zip");
		if (empty($files[0])) {
			die('Failed to download file');
		}
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: private', false);
		header('Content-Transfer-Encoding: binary');
		header('Content-Disposition: attachment; filename="' . basename($files[0]) . '";');
		header('Content-type: application/zip');
		header('Content-Length: ' . filesize($files[0]));

		/* Chunked download */
		$handle = fopen($files[0], 'rb');
		$chunkSize = 8192;
		while (!feof($handle)) {
			$buffer = fread($handle, $chunkSize);
			echo $buffer;
			ob_flush();
			flush();
		}
		fclose($handle);

		unlink($files[0]);
		exit;
	}

	protected function checkPassword()
	{
		if (empty($_GET['p']) || $_GET['p'] !== $this->password) {
			exit;
		}
	}

	protected function echoImmediately($var)
	{
		echo $var;
		ob_flush();
		flush();
	}

	protected function getFileSizeFormatted($path, $decimals = 2)
	{
		$bytes = filesize($path);
		$size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		$factor = floor((strlen($bytes) - 1) / 3);
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
	}
}

class FlxZipArchive extends ZipArchive
{
	/* Add a Dir with Files and Subdirs to the archive */
	public function addDir($location, $name)
	{
		// Don't zip backup tool
		if (basename(__DIR__) == basename($name)) return;

		$this->addEmptyDir($name);
		$this->addDirDo($location, $name);
	}

	/* Add Files & Dirs to archive. */
	private function addDirDo($location, $name)
	{
		$name .= '/';
		$location .= '/';

		// Read all Files in Dir
		$dir = opendir($location);
		while ($file = readdir($dir)) {
			if ($file == '.' || $file == '..') continue;

			// Rekursiv, If dir: FlxZipArchive::addDir(), else ::File();
			$do = (filetype($location . $file) == 'dir') ? 'addDir' : 'addFile';
			$this->$do($location . $file, $name . $file);
		}
	}
}