<?php
@ini_set('max_execution_time', '300');

new Run_Backups_Script;
class Run_Backups_Script
{
    public $files_to_download = array(
        'http://localhost/24so_ec/backup/index.php?p=QTQP5H42m3dKu5AX&silent=true'
    );

    public function __construct()
    {
        $date = date("Y-m-d_H-i-s");
        $backup_folder_path = './'.count($this->files_to_download).' '. $date;
        if (mkdir($backup_folder_path, 0755, true)) {
            $this->multiple_download($this->files_to_download, $backup_folder_path);
            echo 'Backup done for '.count($this->files_to_download).' projects. Date '.$date.'.';
        }
    }

    protected function multiple_download(array $urls, $save_path = '/tmp')
    {
        $multi_handle = curl_multi_init();
        $file_pointers = array();
        $curl_handles = array();
        // Add curl multi handles, one per file we don't already have
        foreach ($urls as $key => $url) {
            $file = $save_path . '/' . $key . '-' . parse_url($url, PHP_URL_HOST) . '.zip';
            if (!is_file($file)) {
                $curl_handles[$key] = curl_init($url);
                $file_pointers[$key] = fopen($file, "w");
                curl_setopt($curl_handles[$key], CURLOPT_FILE, $file_pointers[$key]);
                curl_setopt($curl_handles[$key], CURLOPT_HEADER, 0);
                curl_setopt($curl_handles[$key], CURLOPT_CONNECTTIMEOUT, 60);
                curl_multi_add_handle($multi_handle, $curl_handles[$key]);
            }
        }
        // Download the files
        do {
            curl_multi_exec($multi_handle, $running);
        } while ($running > 0);
        // Free up objects
        foreach ($urls as $key => $url) {
            curl_multi_remove_handle($multi_handle, $curl_handles[$key]);
            curl_close($curl_handles[$key]);
            fclose($file_pointers[$key]);
        }
        curl_multi_close($multi_handle);
    }
}