# PHP Project Backup Tool
# Setup
### backup folder
Upload *backup* folder to the project's root directory, backup of which you want to do. Then config DB credentials:
```php
/* Pure DB */
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "root");
define("DB_NAME", "db");
```
Or if you using Wordpress:
```php
/* WP DB */
define('SHORTINIT', true);
require '../wp-config.php';
```
Don't forget to change the password:
```php
public $password = "QTQP5H42m3dKu5AX";
```

### run-backups folder
Upload *run-backups* on server you want to store all backups. Fill array of urls with yours projects:
```php
public $files_to_download = array(
    'http://localhost/24so_ec/backup/index.php?p=QTQP5H42m3dKu5AX&silent=true'
);
```

### Manul backup url example
*http://localhost/24so_ec/backup/index.php?p=QTQP5H42m3dKu5AX*